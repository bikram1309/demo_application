from django.contrib import admin
from django.urls import path,include
from home import views


urlpatterns = [
     path("",views.index,name='index'),
    path("upload",views.upload,name='upload'),
    path("changes1",views.changes1,name='changes1'),
   path("books",views.book_list,name='book_list'),
    path("books/upload",views.upload_book,name='upload_book'),
    #path("printer1",views.printer1,name='printer1'),

]

