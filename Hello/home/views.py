from django.shortcuts import render,redirect
from django.core.files.storage import FileSystemStorage
from django.conf import settings
import os

# Create your views here.
from .forms import BookForm
from .models import Book,Myfile


COMMAND = {
    'START': 'Print start',
    'CANCEL': 'Print canceled',
    'FAIL': 'Print failed',
    'SUCCESS': 'Print success'
}

def upload(request):
    if request.method == 'POST':
        f_name=request.POST.get('filename')
        uploaded_files = request.FILES.get("document")
        print(f_name)
        inst= Myfile(title=f_name,pdf=uploaded_files)
        inst.save()
        
        
        return render(request, 'changes1.html')
    else:
        return render(request, 'upload.html')
    



def changes1(request):
    

    return render(request, 'changes1.html')

name_printers = os.listdir(path_dir)

def index(request):
    #print(os.environ.get('DEMO'))
    filepath=r'C:\Users\ADMIN\Desktop\demo\Hello'
    path_dir = os.path.join(filepath,'media')
    print(os.getcwd())
    allinfo=Book.objects.all()
    for info in allinfo:
        #print(info.pdf)
        task=info.pdf
        print(task)
        print(os.path.join(path_dir, task))
        #p = os.path.join('static/JP-B-001/20210522_150929_CFFFP_3DBenchy.gcode.log')
        
    return render(request, 'index.html')

def book_list(request):
    books=Book.objects.all()

    
    return render(request, 'book_list.html',{
        'books': books
    })

def upload_book(request):
    if request.method == 'POST':
        form = BookForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('book_list')
    else:
        form = BookForm()
   
   
    return render(request, 'upload_book.html',{
        'form':form
    })


'''
with open(path_log, 'r') as f:
            lines = f.readlines()
        for i, line in enumerate(lines):
                if COMMAND['START'] in line:
                    line = line.strip().split(' ')
                    day = line[0]
                    time = line[1]
                    print(day)
                    print(time)   

'''

